ASM=nasm
CC=gcc

BOOT_TARGET = boot.bin
BOOT_SRCS = boot.S
BOOT_OBJS = $(BOOT_SRCS:.S=.o)
BOOT_FLAGS = -f bin

SRCS = 

OBJS = $(SRCS:.c=.o)

.SUFFIXES: .c .o
.PHONY: all boot clean bochs

all: boot
	
.c.o:
	$(CC) $(CFLAGS) $(INCLUDES) -c $<  -o $@
boot: $(BOOT_SRCS)
	$(ASM) $(BOOT_FLAGS) $< -o $(BOOT_TARGET)
clean:
	-rm -r *.o *.bin 2> /dev/null
bochs: boot
	dd if=$(BOOT_TARGET) of=/img/floppy.img bs=512 count=2880
	bochs
